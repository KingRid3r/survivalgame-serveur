﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitZone : MonoBehaviour
{
    public float damage;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            // Hit a player
            other.GetComponent<Player>().TakeDamage(damage/2);
        }
        
        if (other.gameObject.tag == "Enemy")
        {
            // Hit an ennemy
            other.GetComponent<Enemy>().TakeDamage(damage);
        }
        gameObject.SetActive(false);
    }

    
}
