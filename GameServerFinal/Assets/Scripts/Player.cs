﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int id;
    public string username;
    public CharacterController controller;
    public Transform shootOrigin;
    public GameObject hitZone;
    
    public float gravity = -9.81f;
    public float moveSpeed = 5f;
    public float jumpSpeed = 5f;
    public float throwForce = 600f;
    public float damageForce = 50f;
    public float health;
    public float maxHealth = 100f;
    public int itemAmount = 0;
    public int maxItemAmount = 3;
    public float hitRange = 1f;

    private bool[] inputs;
    private Vector3 position;
    private float yVelocity = 0;

    private void Start()
    {
        gravity *= Time.fixedDeltaTime * Time.fixedDeltaTime;
        moveSpeed *= Time.fixedDeltaTime;
        jumpSpeed *= Time.fixedDeltaTime;
        hitZone.GetComponent<HitZone>().damage = damageForce;
    }

    public void Initialize(int _id, string _username)
    {
        id = _id;
        username = _username;
        health = maxHealth;

        inputs = new bool[5];
    }

    /// <summary>Processes player input and moves the player.</summary>
    public void FixedUpdate()
    {
        
        // if (health <= 0f)
        // {
        //     return;
        // }

        
        
        // Vector2 _inputDirection = Vector2.zero;
        // if (inputs[0])
        // {
        //     _inputDirection.y += 1;
        // }
        // if (inputs[1])
        // {
        //     _inputDirection.y -= 1;
        // }
        // if (inputs[2])
        // {
        //     _inputDirection.x -= 1;
        // }
        // if (inputs[3])
        // {
        //     _inputDirection.x += 1;
        // }

        if (position != null)
        {
            UpdatePosition();
        }
        
    }

    /// <summary>Calculates the player's desired movement direction and moves him.</summary>
    /// <param name="_inputDirection"></param>
    private void Move(Vector2 _inputDirection)
    {
        Vector3 _moveDirection = transform.right * _inputDirection.x + transform.forward * _inputDirection.y;
        _moveDirection *= moveSpeed;

        if (controller.isGrounded)
        {
            yVelocity = 0f;
            if (inputs[4])
            {
                yVelocity = jumpSpeed;
            }
        }
        yVelocity += gravity;

        _moveDirection.y = yVelocity;
        controller.Move(_moveDirection);

        ServerSend.PlayerPosition(this);
        ServerSend.PlayerRotation(this);
    }
    
    /// <summary>Update the player's desired movement direction and moves him.</summary>
    /// <param name="_inputDirection"></param>
    private void UpdatePosition()
    {
        gameObject.transform.position = position;

        ServerSend.PlayerPosition(this);
        ServerSend.PlayerRotation(this);
    }

    /// <summary>Updates the player input with newly received input.</summary>
    /// <param name="_inputs">The new key inputs.</param>
    /// <param name="_rotation">The new rotation.</param>
    public void SetInput(bool[] _inputs, Quaternion _rotation)
    {
        inputs = _inputs;
        transform.rotation = _rotation;
    }
    
    /// <summary>Updates the player position with newly received position.</summary>
    /// <param name="_position">The new key position.</param>
    /// <param name="_rotation">The new rotation.</param>
    public void SetInput2(Vector3 _position, Quaternion _rotation)
    {
        position = _position;
        transform.rotation = _rotation;
    }

    public void Shoot(Vector3 _viewDirection)
    {
        if (health <= 0f)
        {
            return;
        }

        if (Physics.Raycast(shootOrigin.position, _viewDirection, out RaycastHit _hit, hitRange))
        {
            if (_hit.collider.CompareTag("Player"))
            {
                _hit.collider.GetComponent<Player>().TakeDamage(damageForce/2);
            }
            else if (_hit.collider.CompareTag("Enemy"))
            {
                _hit.collider.GetComponent<Enemy>().TakeDamage(damageForce);
            }
        }
    }
    
    
    public void Hit()
    {
        // if (health <= 0f)
        // {
        //     return;
        // }

        hitZone.SetActive(true);
        
        // if (Physics.Raycast(shootOrigin.position, hitZone.transform.position, out RaycastHit _hit, hitRange))
        // {
        //     if (_hit.collider.CompareTag("Player"))
        //     {
        //         _hit.collider.GetComponent<Player>().TakeDamage(damageForce/2);
        //     }
        //     else if (_hit.collider.CompareTag("Enemy"))
        //     {
        //         _hit.collider.GetComponent<Enemy>().TakeDamage(damageForce);
        //     }
        // }
    }

    public void ThrowItem(Vector3 _viewDirection)
    {
        if (health <= 0f)
        {
            return;
        }

        if (itemAmount > 0)
        {
            itemAmount--;
            NetworkManager.instance.InstantiateProjectile(shootOrigin).Initialize(_viewDirection, throwForce, id);
        }
    }

    public void TakeDamage(float _damage)
    {
        if (health <= 0f)
        {
            return;
        }

        health -= _damage;
        // if (health <= 0f)
        // {
        //     health = 0f;
        //     controller.enabled = false;
        //     transform.position = new Vector3(0f, 25f, 0f);
        //     ServerSend.PlayerPosition(this);
        //     StartCoroutine(Respawn());
        // }

        ServerSend.PlayerHealth(this);
    }

    private IEnumerator Respawn()
    {
        yield return new WaitForSeconds(5f);

        health = maxHealth;
        controller.enabled = true;
        ServerSend.PlayerRespawned(this);
    }

    public bool AttemptPickupItem()
    {
        if (itemAmount >= maxItemAmount)
        {
            return false;
        }

        itemAmount++;
        return true;
    }
}
